Ext.define('Evaluator.view.Main', {
    extend: 'Ext.Viewport',
    alias: 'mainview',
    // controller: 'mainviewcontroller',
    // viewModel: {
    //     type: 'mainviewmodel'
    // },
    // requires: [
    //     'Ext.layout.Fit'
    // ],
    layout: 'border',
    defaults: {
        split: true
    },
    // items: [
    //     { xtype: 'navview',    reference: 'navview',    docked: 'left',   bind: {width:  '{navview_width}'}, listeners: { select: "onMenuViewSelectionChange"} },
    //     { xtype: 'headerview', reference: 'headerview', docked: 'top',    bind: {height: '{headerview_height}'} },
    //     { xtype: 'footerview', reference: 'footerview', docked: 'bottom', bind: {height: '{footerview_height}'} },
    //     { xtype: 'centerview', reference: 'centerview' },
    //     { xtype: 'detailview', reference: 'detailview', docked: 'right',  bind: {width:  '{detailview_width}'}  },
    // ],
    initComponent: function () {
        this.items = [
            {
                xtype: 'panel',
                html: 'This is north kek panel',
                region: 'north',
            },
            {
                xtype: 'panel',
                html: 'This is west kek panel',
                region: 'west',
            },
            {
                xtype: 'panel',
                html: 'This is east kek panel',
                region: 'east',
            },
            {
                xtype: 'panel',
                html: 'This is south kek panel',
                region: 'south',
            }
        ]
    }
});
