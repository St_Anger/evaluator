Ext.define('Evaluator.Application', {
    extend: 'Ext.app.Application',
    name: 'Evaluator',
    requires: [
        'Evaluator.view.Main',
    ],

    launch: function () {
        // var whichView = 'mainview';
        Ext.Viewport.add([{xtype: 'mainview'}])
    }
});
