<?php

namespace App\Controller;

use App\Model\DataManager\DataManager;
use App\Model\Evaluator\CalculationParamsContainer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;


class UserInterfaceController extends AbstractController
{

    public function index(EntityManagerInterface $entityManager)
    {

        dump($entityManager->getConnection());die;
        return $this->render('user_interface/index.html.twig', [
            'controller_name' => 'UserInterfaceController',
        ]);
    }

    public function crud(Request $request, DataManager $dataManager) {

        /**
         * Формат данных для формы на фронте
         * $estimation = [
         *      'caption' => $estimationCaption,
         *      'oilPrice' => $oilPrice,
         *      'met' => $met,
         *      'discountRate' => $discountRate
         * ]
         */
        $estimation = $request->get('estimationParams') || $request->query->get('estimationParams');
        /**
         * Формат данных для таблицы фронта (не придумал как отвязаться от полей таблицы на фронте)
         * $wellsEconomicHistoryData = [
         *      0 => [
         *          'uniqueName' => $uniqueName,
         *          'capex' => $capex,
         *          '2018' => [
         *                  'year' => $year,
         *                  'oilProduction' => $oilProduction,
         *                  'opex' => $opex
         *                  ],
         *          '2019' => [
         *                  'year' => $year,
         *                  'oilProduction' => $oilProduction,
         *                  'opex' => $opex
         *                  ],
         */
        $wellsEconomicHistoryData = $request->get('wellsEconomic') || $request->query->get('wellsEconomic');
        switch ($request->getMethod()) {
            case Request::METHOD_GET:

                break;
            case Request::METHOD_POST:
                $dataManager->createEvaluation($wellsEconomicHistoryData, $estimation);

                break;
            case Request::METHOD_PUT:
                $macroParams = $request->get('macroParams');
                $calculationCaption = $request->get('calculationUniqueName');
                break;
            case Request::METHOD_DELETE:
                break;
        }
    }
}
