<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table("WellEconomicHistory")
 */
class WellEconomicHistory
{
    /**
     * @ORM\Id()
     * @ORM\SequenceGenerator(sequenceName="WellEconomicHistory_seq")
     * @ORM\Column(name="id", type="integer", nullable=false)
     */
    private $id;

    /**
     * Версия рассчёта
     * @var Estimation
     * @ORM\ManyToOne(targetEntity="Estimation")
     * @ORM\JoinColumn(name="estimation", referencedColumnName="id")
     */
    private $estimation;

    /**
     * @ORM\Column(name="year", type="date", nullable=false)
     */
    private $year;

    /**
     * @ORM\Column(name="oilproduction", type="float", nullable=true)
     */
    private $oilProduction;

    /**
     * Operational cost per unit oil production
     * @ORM\Column(name="opex", type="float", nullable=true)
     */
    private $opex;

    /**
     * @var Well
     * @ORM\ManyToOne(targetEntity="Well")
     * @ORM\JoinColumn(name="well", referencedColumnName="id")
     */
    private $well;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param mixed $year
     * @return $this
     */
    public function setYear($year)
    {
        $this->year = $year;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOilProduction()
    {
        return $this->oilProduction;
    }

    /**
     * @param mixed $oilProduction
     * @return $this
     */
    public function setOilProduction($oilProduction)
    {
        $this->oilProduction = $oilProduction;
        return $this;
    }

    /**
     * @return Estimation
     */
    public function getEstimation()
    {
        return $this->estimation;
    }

    /**
     * @param Estimation $estimation
     * @return $this
     */
    public function setEstimation($estimation)
    {
        $this->estimation = $estimation;
        return $this;
    }

    /**
     * @return Well
     */
    public function getWell()
    {
        return $this->well;
    }

    /**
     * @param Well $well
     * @return $this
     */
    public function setWell($well)
    {
        $this->well = $well;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOpex()
    {
        return $this->opex;
    }

    /**
     * @param mixed $opex
     * @return $this
     */
    public function setOpex($opex)
    {
        $this->opex = $opex;
        return $this;
    }

}