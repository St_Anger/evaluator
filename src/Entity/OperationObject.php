<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="OperationObjects")
 */
class OperationObject
{
    /**
     * @ORM\Id()
     * @ORM\SequenceGenerator(sequenceName="OperationObjects_seq")
     * @ORM\Column(name="id", type="integer", nullable=false)
     */
    private $id;

    /**
     * @ORM\Column(name="caption", type="string", length=50, nullable=false)
     */
    private $caption;

    /**
     * @ORM\ManyToMany(targetEntity="Well", mappedBy="operationObjects")
     */
    private $wells;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCaption()
    {
        return $this->caption;
    }

    /**
     * @param mixed $caption
     * @return $this
     */
    public function setCaption($caption)
    {
        $this->caption = $caption;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWells()
    {
        return $this->wells;
    }

    /**
     * @param mixed $wells
     * @return $this;
     */
    public function setWells($wells)
    {
        $this->wells = $wells;
        return $this;
    }


}