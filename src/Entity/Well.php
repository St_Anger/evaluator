<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\Table("Wells")
 */
class Well
{
    /**
     * @ORM\Id()
     * @ORM\SequenceGenerator(sequenceName="well_seq")
     * @ORM\Column(name="id", type="integer", nullable=false)
     */
    private $id;

    /**
     * unique name of Well
     * @Assert\Unique()
     * @ORM\Column(name="uniquename", type="string", length=50)
     */
    private $uniqueName;

    /**
     * Annual CAPEX for well
     * @ORM\Column(name="capex", type="float", nullable=true)
     */
    private $capex;

    /**
     * @var Field
     * @ORM\ManyToOne(targetEntity="Field")
     * @ORM\JoinColumn(name="field", referencedColumnName="id")
     */
    private $field;

    /**
     * @ORM\ManyToMany(targetEntity="Well", inversedBy="OperationObject")
     * @ORM\JoinTable(name="WellOperationObjectRelation",
     *      joinColumns={@ORM\JoinColumn(name="wellid", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="operationobjectid", referencedColumnName="id")}
     *      )
     */
    private $operationObjects;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUniqueName()
    {
        return $this->uniqueName;
    }

    /**
     * @param mixed $uniqueName
     * @return $this
     */
    public function setUniqueName($uniqueName)
    {
        $this->uniqueName = $uniqueName;
        return $this;
    }

    /**
     * @return Field
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param Field $field
     * @return $this
     */
    public function setField($field)
    {
        $this->field = $field;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOperationObjects()
    {
        return $this->operationObjects;
    }

    /**
     * @param mixed $operationObjects
     * @return $this
     */
    public function setOperationObjects($operationObjects)
    {
        $this->operationObjects = $operationObjects;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCapex()
    {
        return $this->capex;
    }

    /**
     * @param mixed $capex
     * @return $this
     */
    public function setCapex($capex)
    {
        $this->capex = $capex;
        return $this;
    }



}