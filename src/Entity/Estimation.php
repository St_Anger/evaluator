<?php


namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;

class Estimation
{
    /**
     * @ORM\Id()
     * @ORM\SequenceGenerator(sequenceName="Estimation_seq")
     * @ORM\Column(name="id", type="integer", nullable=false)
     */
    private $id;

    /**
     * @ORM\Column(name="caption", type="string", length=200, options={"comment":"Name of the saved specific evaluation"})
     */
    private $caption;

    /**
     * @ORM\Column(name="oilprice", type="float", nullable=false)
     */
    private $oilPrice;

    /**
     * @ORM\Column(name="met", type="float", nullable=true, options={"comment":"Mineral extraction tax, %"})
     */
    private $met;

    /**
     * @ORM\Column(name="discountrate", type="float", nullable=true)
     */
    private $discountRate;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCaption()
    {
        return $this->caption;
    }

    /**
     * @param mixed $caption
     * @return $this
     */
    public function setCaption($caption)
    {
        $this->caption = $caption;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOilPrice()
    {
        return $this->oilPrice;
    }

    /**
     * @param mixed $oilPrice
     * @return $this
     */
    public function setOilPrice($oilPrice)
    {
        $this->oilPrice = $oilPrice;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMet()
    {
        return $this->met;
    }

    /**
     * @param mixed $met
     * @return $this
     */
    public function setMet($met)
    {
        $this->met = $met;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDiscountRate()
    {
        return $this->discountRate;
    }

    /**
     * @param mixed $discountRate
     * @return $this
     */
    public function setDiscountRate($discountRate)
    {
        $this->discountRate = $discountRate;
        return $this;
    }


}