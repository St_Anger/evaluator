<?php


namespace App\Model\DataManager;

use App\Entity\Estimation;
use App\Entity\Well;
use App\Entity\WellEconomicHistory;
use Doctrine\ORM\EntityManagerInterface;

class DataManager
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function createEvaluation($wellsEconomicHistoryData, $estimation) {
        $estimationEntity = $this->createEstimation($estimation);
        $wellRepository = $this->entityManager->getRepository(Well::class);
        foreach ($wellsEconomicHistoryData as $wellEconomicHistoryData) {
            $wellUniqueName = $wellEconomicHistoryData['uniqueName'];
            $wellCapex = $wellsEconomicHistoryData['capex'];
            $wellEntity = $wellRepository->findOneBy(['uniqueName' => $wellUniqueName]) ?? null;
            if (empty($wellEntity)) {
                file_put_contents(__DIR__ . 'DataManagerLog.log', $this->getLogMsg($wellUniqueName), FILE_APPEND);
                continue;
            }
            $wellEntity->setCapex($wellCapex);
            $this->$this->entityManager->persist($wellEntity);
            foreach ($wellsEconomicHistoryData as $key => $value) {
                if (is_numeric($key)) {
                    $opex = $value['opex'];
                    $oilProduction = $value['oilProduction'];
                    $wellEconomicEntity = (new WellEconomicHistory())
                        ->setWell($wellEntity)
                        ->setOpex($opex)
                        ->setOilProduction($oilProduction)
                        ->setEstimation($estimationEntity);
                    $this->entityManager->persist($wellEconomicEntity);
                }
            }
        }
        $this->entityManager->flush();
    }

    public function getEvaluation($wells, $estimation) {
        
    }

    private function createEstimation($estimation) {
        $estimationEntity = (new Estimation())
            ->setCaption($estimation['caption'])
            ->setDiscountRate($estimation['discountRate'])
            ->setMet($estimation['met'])
            ->setOilPrice($estimation['oilPrice']);
        $this->entityManager->persist($estimationEntity);
        return $estimationEntity;
    }

    private function getLogMsg($wellUniqueName) {
        return 'Well with uniqueName ' . $wellUniqueName . 'is not found. Data skipped' . PHP_EOL;
    }


}