<?php


namespace App\Model\Evaluator;


use App\Entity\Field;
use App\Entity\OperationObject;
use App\Entity\Well;
use App\Entity\WellEconomicHistory;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class Evaluator
 * Имеет три публичных метода, оценивающих NPV соответствующих объектов
 * Оперирует соответствующими Entity
 * TODO по-хорошему надо вынести логику извлечения данных в DataManager. Сделаю, если успею
 */
class Evaluator
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    public function calcWellNPV($well, $estimation) {
        $wells[0] = $well;
        $wellNPV = $this->calcWellSetNPV($wells, $estimation);
        return $wellNPV;
    }

    public function calcOperationObjectNPV(OperationObject $operationObject, $estimation) {
        $wells = $operationObject->getWells();
        $operationObjectNPV = $this->calcWellSetNPV($wells, $estimation);
        return $operationObjectNPV;
    }

    public function calcFieldNPV(Field $field, $estimation) {
        $wellRepository = $this->entityManager->getRepository(Well::class);
        $fieldId = $field->getId();
        $wells = $wellRepository->findBy(['field' => $fieldId]);
        $fieldNPV = $this->calcWellSetNPV($wells, $estimation);
        return $fieldNPV;
    }



    private function calcWellSetNPV(array $wells, $estimation) {
        $wellSetNPV = 0;
        foreach ($wells as $well) {
            $wellSetNPV = $wellSetNPV + $this->calcAllHistWellNPV($well, $estimation);
        }
        return $wellSetNPV;
    }

    private function calcAllHistWellNPV(Well $well, $estimation) {
        $wellNPV = 0;
        $wellId = $well->getId();
        $yearCounter = 0;

        $wellEconomicRepository = $this->entityManager->getRepository(WellEconomicHistory::class);
        $wellEconomic = $wellEconomicRepository->findBy(['well' => $wellId, 'estimation.caption' => $estimation], ['year' => 'ASC']);
        /**
         * @var WellEconomicHistory $wellYearData
         */
        foreach ($wellEconomic as $wellYearData) {
            $discountMultiplier = $this->getDiscountMultiplier($wellYearData, $yearCounter);
            $wellNPV = $wellNPV + $discountMultiplier * ($this->calcWellCF($wellYearData) - $well->getCapex());
            $yearCounter++;
        }
        return $wellNPV;
    }

    /**
     * Считаем множитель дисконтирования в зависимости от прошедшего времени (в годах) с начала разработки
     * @param WellEconomicHistory $wellYearData
     * @param $yearCounter
     * @return float|int
     */
    private function getDiscountMultiplier($wellYearData, $yearCounter) {
        $discountRate = $wellYearData->getEstimation()->getDiscountRate();
        return 1/pow(1 + $discountRate, $yearCounter);
    }

    /**
     * Считаем показатель CF для скважины
     * @param WellEconomicHistory $wellYearData
     * @return float|int
     */
    private function calcWellCF($wellYearData) {
        $oilPrice = $wellYearData->getEstimation()->getOilPrice();
        $met = $wellYearData->getEstimation()->getMet(); // ставка НДПИ
        $gain = $oilPrice * $wellYearData->getOilProduction();
        $opex = $wellYearData->getOilProduction() * $wellYearData->getOpex();
        $ndpi = $gain * $met;
        $cf = $gain - $opex - $ndpi;
        return $cf;
    }
}