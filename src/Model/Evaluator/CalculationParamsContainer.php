<?php


namespace App\Model\Evaluator;


class CalculationParamsContainer
{
    private $oilPrice;
    /**
     * Mineral extraction tax
     * Налог на добычу полезных ископаемых
     * @var mixed
     */
    private $met;

    /**
     * Ставка дисконтирования
     * @var mixed
     */
    private $discountRate;

    /**
     * Название вычисления
     * @var string
     */
    private $calculationCaption;

    public function __construct($macroParams, $calculationCaption)
    {
        $this->oilPrice = $macroParams['oilPrice'];
        $this->met = $macroParams['met'];
        $this->discountRate = $macroParams['discountRate'];
        $this->calculationCaption = $calculationCaption;
    }

    /**
     * @return mixed
     */
    public function getOilPrice()
    {
        return $this->oilPrice;
    }

    /**
     * @param mixed $oilPrice
     */
    public function setOilPrice($oilPrice): void
    {
        $this->oilPrice = $oilPrice;
    }

    /**
     * @return mixed
     */
    public function getMet()
    {
        return $this->met;
    }

    /**
     * @param mixed $met
     */
    public function setMet($met): void
    {
        $this->met = $met;
    }

    /**
     * @return mixed
     */
    public function getDiscountRate()
    {
        return $this->discountRate;
    }

    /**
     * @param mixed $discountRate
     */
    public function setDiscountRate($discountRate): void
    {
        $this->discountRate = $discountRate;
    }

}